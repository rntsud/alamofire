Pod::Spec.new do |s|
  s.name = 'Alamofire-Vesti'
  s.version = '5.0.3'
  s.license = 'MIT'
  s.summary = 'Elegant HTTP Networking in Swift'
  s.homepage = 'https://bitbucket.org/rntsud/alamofire'
  s.social_media_url = 'http://twitter.com/AlamofireSF'
  s.authors = { 'Alamofire Software Foundation' => 'info@alamofire.org' }
  s.source = { :git => 'https://rntsud:x9XDg6W2vKMZcFQ9STJp@bitbucket.org/rntsud/alamofire.git', :tag => s.version }

  s.ios.deployment_target = '10.0'
  s.osx.deployment_target = '10.12'
  s.tvos.deployment_target = '10.0'
  s.watchos.deployment_target = '3.0'

  s.source_files = 'Source/*.swift'

  s.frameworks = 'CFNetwork'
end
